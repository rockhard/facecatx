import 'package:flutter/material.dart';

import 'app.dart';
import 'di.dart';

Future<void> main() async {
  await DependencyInjection.setUp();

  runApp(FaceCatXApp());
}
