import 'dart:math';

import 'package:facecatx/model/chat.dart';
import 'package:facecatx/model/message.dart';
import 'package:facecatx/view/pages/chat/chat_card.dart';
import 'package:facecatx/view/pages/chat_list/chat_list_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'chat_bloc.dart';

class ChatScreen extends StatefulWidget {
  static const String route = '/chat';

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  //ChatBloc _chatBloc;

  @override
  void initState() {
    //_chatBloc = BlocProvider.of<ChatBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    final cardSide = min(width, height);
    final Chat chat = ModalRoute.of(context).settings.arguments;

    return BlocBuilder<ChatBloc, ChatState>(
      builder: (context, state) {
        var safePadding = MediaQuery.of(context).padding.top;

        return Material(
          child: CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                backgroundColor: Colors.white,
                expandedHeight: cardSide,
                floating: false,
                pinned: true,
                flexibleSpace: LayoutBuilder(
                  builder: (BuildContext context, BoxConstraints constraints) {
                    return ChatCard(
                      chat: chat,
                      cardSide: cardSide + safePadding,
                      constraints: constraints,
                    );
                  },
                ),
                actions: [
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: InkWell(
                      customBorder: CircleBorder(),
                      onTap: () {
                        print("options");
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(Icons.more_vert),
                      ),
                    ),
                  ),
                ],
              ),
              SliverList(
                delegate: SliverChildBuilderDelegate(
                  (context, index) {
                    final message = chat.messages[index];
                    return _renderMessage(message);
                  },
                  childCount: chat.messages.length,
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  Widget _renderMessage(Message message) {
    return ListTile(
      contentPadding: EdgeInsets.only(top: 8, bottom: 0, left: 16, right: 16),
      leading: (message.avatarUrl != null)
          ? SizedBox(
              width: 50,
              height: 100,
              child: Image.network(
                message.avatarUrl,
                fit: BoxFit.contain,
              ),
            )
          : SizedBox(height: 0, width: 0),
      title: _renderMessageTitle(message),
      subtitle: _renderMessageBody(message),
      trailing: Text(
        message.formattedTime,
        style: TextStyle(color: Colors.grey),
      ),
    );
  }

  Widget _renderMessageTitle(Message message) {
    return Text(message.name,
        style: TextStyle(fontSize: 17, fontWeight: FontWeight.w900));
  }

  Widget _renderMessageBody(Message message) {
    if (message.isImage) {
      return Padding(
        padding: const EdgeInsets.only(top: 4.0),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(8),
          child: Image.network(
            message.photoUrl,
            fit: BoxFit.contain,
          ),
        ),
      );
    }

    if (message.isSticker) {
      return Padding(
        padding: const EdgeInsets.only(top: 4.0),
        child: Container(
            alignment: Alignment.topLeft,
            padding: EdgeInsets.only(top: 4),
            child: SizedBox(
              width: 150,
              height: 150,
              child: Center(
                child: Text("Sticker here"),
              ),
            )),
      );
    }

    return Padding(
      padding: const EdgeInsets.only(top: 4.0),
      child: Text(
        message.text,
        style: TextStyle(
            color: Colors.black87, fontSize: 17, fontWeight: FontWeight.normal),
      ),
    );
  }
}
