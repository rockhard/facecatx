import 'package:auto_size_text/auto_size_text.dart';
import 'package:facecatx/model/chat.dart';
import 'package:flutter/material.dart';
import 'package:facecatx/util/colors.dart';

class ChatCard extends StatelessWidget {
  const ChatCard({
    Key key,
    @required this.chat,
    @required this.cardSide,
    this.constraints,
  }) : super(key: key);

  final Chat chat;
  final double cardSide;
  final BoxConstraints constraints;

  double get percent {
    if (constraints != null) {
      double percent =
          ((constraints.maxHeight - cardSide) * 100 / (100 - cardSide));
      print("${percent}");
      if (percent >= 0 && percent <= 100) {
        return percent;
      } else {
        return 100;
      }
      double dx = 0;

      dx = 100 - percent;
      if (constraints.maxHeight == 100) {
        dx = 0;
      }
    }
    return 100;
  }

  double get opacity {
    if (constraints != null) {
      return (100 - percent) / 100;
    } else {
      return 1.0;
    }
  }

  @override
  Widget build(BuildContext context) {

    Size size = MediaQuery.of(context).size;

    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [chat.bg.toPrimaryColor, chat.bg.toSecondaryColor],
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
        ),
      ),
      width: cardSide,
      height: cardSide,
      child: Stack(
        children: <Widget>[
          if (chat.cover != null)
            Image.network(
              chat.cover,
              height: cardSide,
              fit: BoxFit.cover,
              alignment: Alignment.bottomRight,
              //repeat: ImageRepeat.repeat,
            ),
          Opacity(
            opacity: opacity,
            child: Center(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 32),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    if (chat.relevance != null)
                      Text(
                        chat.relevance,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.w300),
                      ),
                    Divider(height: 4, color: Colors.transparent),
                    AutoSizeText(
                      "${chat.name}",
                      textAlign: TextAlign.center,
                      minFontSize: 12,
                      maxFontSize: 32,
                      maxLines: 4,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 32,
                          fontWeight: FontWeight.w800),
                    ),
                    Divider(height: 16, color: Colors.transparent),
                  ],
                ),
              ),
            ),
          ),

          Align(
            alignment: Alignment.bottomLeft,
            child: Container(
              height: 80,
              child: Stack(
                children: [
                  Opacity(
                    opacity: 1.0 - opacity,
                    child: Align(
                        alignment: Alignment(0, 0.3),
                        child: Container(
                          width: size.width * 0.8,
//                          height: 75,
                          child: Text(
                            chat.name,
                            textAlign: TextAlign.center,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 19,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        )),
                  ),
                  Opacity(
                    opacity: opacity,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(width: 16),
                        if (chat.avatarUrl != null)
                          Image.network(
                            chat.avatarUrl,
                            fit: BoxFit.contain,
                          ),
                        Text(
                          "${chat.authorName}",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        Spacer(),
                        Text(
                          "${chat.messagesCount} ответов",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.w300,
                              fontFeatures: []),
                        ),
                        SizedBox(
                          width: 24,
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
