import 'dart:math';
import 'dart:ui';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:facecatx/model/chat.dart';
import 'package:facecatx/service/facecat/facecat_api.dart';
import 'package:facecatx/view/pages/chat/chat_card.dart';
import 'package:facecatx/view/pages/chat/chat_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:facecatx/util/colors.dart';
import 'package:facecatx/di.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'chat_list_bloc.dart';

class ChatListPage extends StatefulWidget {
  @override
  _ChatListPageState createState() => _ChatListPageState();
}

class _ChatListPageState extends State<ChatListPage>
    with AutomaticKeepAliveClientMixin {
  final _scrollController = ScrollController();
  final scrollTreshold = 200.0;
  ChatListBloc _chatListBloc;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
    _chatListBloc = BlocProvider.of<ChatListBloc>(context);
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= scrollTreshold) {
      _chatListBloc.add(LoadChatList());
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChatListBloc, ChatListState>(
      builder: (context, state) {
        if (state is ChatListInitial) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else if (state is ChatListFailure) {
          return _renderFailureState("Feed loading failure");
        } else if (state is ChatListSuccess) {
          if (state.pages.isEmpty) {
            return _renderFailureState("Empty data");
          }

          return CustomScrollView(
            controller: _scrollController,
            physics: const BouncingScrollPhysics(
                parent: AlwaysScrollableScrollPhysics()),
            slivers: <Widget>[
              CupertinoSliverRefreshControl(
                onRefresh: () {
                  _chatListBloc.add(ReloadChatList());
                  return;
                },
              ),
              SliverList(
                delegate: SliverChildBuilderDelegate(
                  (context, index) {
                    final chats = state.pages[index].chats;
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: chats.map((chat) => _renderChat(chat)).toList(),
                    );
                  },
                  childCount: state.pages.length,
                ),
              ),
            ],
          );
        }

        return null;
      },
    );
  }

  Widget _renderFailureState(String text) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            text,
            style: TextStyle(
              fontSize: 17,
            ),
          ),
          Divider(
            height: 8,
            color: Colors.transparent,
          ),
          RaisedButton(
            color: appPinkColor,
            textColor: Colors.white,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            child: Text("Reload"),
            onPressed: () {
              _chatListBloc.add(ReloadChatList());
            },
          ),
        ],
      ),
    );
  }

  Widget _renderChat(Chat chat) {
    final size = MediaQuery.of(context).size;
    double cardSide = min(size.width, size.height) * 0.9;

    return Container(
      child: InkWell(
        onTap: () {
          print("tapped ${chat.name}");
          Navigator.of(context).pushNamed(ChatScreen.route, arguments: chat);
        },
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
          child: Center(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(16),
              child: ChatCard(chat: chat, cardSide: cardSide),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  void updateKeepAlive() {}

  @override
  bool get wantKeepAlive => true;
}

