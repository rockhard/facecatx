part of 'chat_list_bloc.dart';

abstract class ChatListState extends Equatable {
  const ChatListState();

  @override
  List<Object> get props => [];
}

class ChatListInitial extends ChatListState {}

class ChatListFailure extends ChatListState {}

class ChatListSuccess extends ChatListState {
  final List<ChatPage> pages;

  String nextFrom() {
    return pages.last.nextFrom;
  }

  const ChatListSuccess({this.pages});

  ChatListSuccess copyWith({List<ChatPage> pages}) {
    return ChatListSuccess(
        pages: pages ?? this.pages
    );
  }

  @override
  List<Object> get props => [pages];

  @override
  String toString() => "ChatListSuccess ${pages} ";
}

