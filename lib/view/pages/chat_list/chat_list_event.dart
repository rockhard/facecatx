part of 'chat_list_bloc.dart';

abstract class ChatListEvent extends Equatable {
  const ChatListEvent();

  @override
  List<Object> get props => [];
}

class LoadChatList extends ChatListEvent {}
class ReloadChatList extends ChatListEvent {}
