import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:facecatx/di.dart';
import 'package:facecatx/model/chat_page.dart';
import 'package:facecatx/service/facecat/facecat_api.dart';

part 'chat_list_event.dart';
part 'chat_list_state.dart';

class ChatListBloc extends Bloc<ChatListEvent, ChatListState> {
  ChatListBloc() : super(ChatListInitial());

  @override
  Stream<Transition<ChatListEvent, ChatListState>> transformEvents(
      Stream<ChatListEvent> events, transitionFn) {
    return super.transformEvents(events, transitionFn);
  }

  @override
  Stream<ChatListState> mapEventToState(ChatListEvent event) async* {
    if (event is LoadChatList) {
      yield* _mapLoadChatListToState();
    }

    if (event is ReloadChatList) {
      yield* _mapReloadChatListToState();
    }
  }

  Stream<ChatListState> _mapReloadChatListToState() async* {
    final api = locator.get<FacecatApi>();
    final currentState = state;

    try {
      if (currentState is ChatListInitial ||
          currentState is ChatListSuccess ||
          currentState is ChatListFailure) {
        final page = await api.getChatPage();
        yield ChatListSuccess(pages: [page]);
      }
    } catch (_) {
      yield ChatListFailure();
    }
  }

  Stream<ChatListState> _mapLoadChatListToState() async* {
    final api = locator.get<FacecatApi>();
    final currentState = state;

    try {

      if (currentState is ChatListInitial) {
        final page = await api.getChatPage();
        yield ChatListSuccess(pages: [page]);
      }

      if (currentState is ChatListSuccess) {
        final page = await api.getChatPage(nextFrom: currentState.nextFrom());
        yield ChatListSuccess(
          pages: currentState.pages + [page],
        );
      }

    } catch (_) {
      yield ChatListFailure();
    }
  }

  bool _hasNext(ChatListState state) =>
      state is ChatListSuccess && state.nextFrom() != null;
}
