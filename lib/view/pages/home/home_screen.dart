import 'dart:math';

import 'package:facecatx/util/colors.dart';
import 'package:facecatx/view/pages/chat_list/chat_list_page.dart';
import 'package:facecatx/view/pages/home/appbar.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  static const String route = '/';

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  PageController _pageController = PageController(initialPage: 1);
  ScrollController _scrollController = ScrollController(keepScrollOffset: true);

  double offset = 0;

  final List<WidgetInfo> pages = [
    WidgetInfo(
      title: 'Profile',
      key: GlobalKey(),
      widget: Center(
        child: Text('Profile will be here'),
      ),
    ),
    WidgetInfo(
      title: 'Feed',
      key: GlobalKey(),
      widget: ChatListPage(),
    ),
    WidgetInfo(
      title: 'Talks',
      key: GlobalKey(),
      widget: Center(
        child: Text("Talks will be here"),
      ),
    ),
  ];

  @override
  void initState() {
    //_scrollController.addListener(() {});
    super.initState();
  }

  @override
  build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        selected: _pageController.page.toInt(),
        offset: offset,
        onPageSelected: (index) {
          _pageController.animateToPage(
            index,
            curve: Curves.easeOut,
            duration: const Duration(milliseconds: 300),
          );
        },
        scrollController: _scrollController,
        pageKeys: pages.map((e) => e.key).toList(),
        pageNames: pages.map((e) => e.title).toList(),
      ),
      body: NotificationListener<ScrollUpdateNotification>(
        onNotification: (notification) {
          if (notification is ScrollNotification) {
            _onPageViewScroll(notification.metrics);
          }
          return;
        },
        child: PageView(
          physics: BouncingScrollPhysics(),
          controller: _pageController,
          children: pages.map((e) => e.widget).toList(),
        ),
      ),
//      floatingActionButton: FloatingActionButton(
//        backgroundColor: appPinkColor,
//        onPressed: () {
////          final keys = pages.values.map((e) => e.key);
//////          final key = keys.last;
//////          final first = keys.first;
////          final randInt = Random().nextInt(keys.length);
////          final key = keys.toList()[randInt];
////          Scrollable.ensureVisible(key.currentContext);
//        },
//        child: Icon(Icons.add),
//      ),
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _pageController.dispose();
    super.dispose();
  }

  _onPageViewScroll(ScrollMetrics scrollMetrics) {
    //print("Scroll ${scrollMetrics.printSelf}");

    final widths = pages.map((e) => e.getWidgetSize().width).toList();
    //print(widths);

    final currentPage = _pageController.page;
    final intPage = currentPage.toInt();
    final pageProgress = currentPage - intPage;
    final titleSize = pages.toList()[intPage].getWidgetSize();

    //final pixels = _pageController.position.pixels;

    final prevPagesWidth = widths
        .asMap()
        .entries
        .where((element) => element.key < intPage)
        .toList()
        .fold(0, (a, e) => a + e.value);

    final offset = prevPagesWidth + (pageProgress * titleSize.width);

    print(offset);
    setState(() {
      this.offset = offset;
    });

//    _scrollController.animateTo(offset, duration: Duration(microseconds: 100), curve: Curves.linear);
    //_scrollController.jumpTo(offset);
  }
}

class WidgetInfo {
  final String title;
  final Widget widget;
  final GlobalKey key;

  WidgetInfo({this.key, this.title, this.widget});

  Size getWidgetSize() {
    return key.getWidgetSize();
  }
}

extension on GlobalKey {
  Size getWidgetSize() {
    final RenderBox renderBoxRed = this.currentContext.findRenderObject();
    final size = renderBoxRed.size;
    return size;
  }
}

extension on ScrollMetrics {
  String get printSelf {
    return "minScrollExtent: $minScrollExtent " +
        "maxScrollExtent: $maxScrollExtent " +
        "pixels: $pixels " +
        "viewportDimension: $viewportDimension " +
        "axisDirection: $axisDirection ";
  }
}
