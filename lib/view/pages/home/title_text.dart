import 'package:flutter/material.dart';

class TitleText extends StatelessWidget {
  final String text;
  final bool selected;

  const TitleText({Key key, @required this.text, @required this.selected}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 8),
      child: Text(
        text,
        style: TextStyle(
          color: selected ? Colors.black : Colors.black.withOpacity(0.6),
          fontSize: 42,
          fontWeight: FontWeight.w700,
        ),
      ),
    );
  }
}
