import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'title_text.dart';

class CustomAppBar extends AnimatedWidget implements PreferredSizeWidget {
  CustomAppBar(
      {Key key,
      this.selected,
      this.onPageSelected,
      this.offset,
      this.scrollController,
      this.pageNames,
      this.pageKeys})
      : super(listenable: scrollController);

  final void Function(int) onPageSelected;
  final ScrollController scrollController;
  final List<String> pageNames;
  final List<GlobalKey> pageKeys;
  final int selected;
  final double offset;

  @override
  final Size preferredSize = Size(0.0, 100.0);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 50, bottom: 8),
      child: _renderAppBar(),
    );
  }

  Widget _renderAppBar() {
    return Stack(
      children: [
        Positioned(
          left: -offset + 8,
          top: 0,
          child: Row(
            children: List.generate(pageNames.length, (index) {
              return InkWell(
                highlightColor: Colors.transparent,
                splashColor: Colors.transparent,
                borderRadius: BorderRadius.circular(8),
                child: TitleText(
                  key: pageKeys[index],
                  selected: index == selected ?? false,
                  text: pageNames[index],
                ),
                onTap: () {
                  if (onPageSelected != null) {
                    onPageSelected(index);
                  }
                },
              );
            }),

//              child: ListView.builder(
//                physics: NeverScrollableScrollPhysics(),
//                itemBuilder: (context, index) {
//                  return InkWell(
//                    highlightColor: Colors.transparent,
//                    splashColor: Colors.transparent,
//                    borderRadius: BorderRadius.circular(8),
//                    child: TitleText(
//                      key: pageKeys[index],
//                      selected: index == selected ?? false,
//                      text: pageNames[index],
//                    ),
//                    onTap: () {
//                      if (onPageSelected != null) {
//                        onPageSelected(index);
//                      }
//                    },
//                  );
//                },
//                scrollDirection: Axis.horizontal,
//                controller: scrollController,
//                itemCount: pageNames.length,
//              ),
          ),
        ),
      ],
    );
  }
}
