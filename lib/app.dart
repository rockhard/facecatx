import 'package:facecatx/api.dart';
import 'package:facecatx/model/chat.dart';
import 'package:facecatx/view/pages/chat/chat_screen.dart';
import 'package:facecatx/view/pages/chat/chat_bloc.dart';
import 'package:facecatx/view/pages/chat_list/chat_list_bloc.dart';
import 'package:facecatx/view/pages/chat_list/chat_list_page.dart';
import 'package:facecatx/view/pages/home/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FaceCatXApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) =>
          ChatBloc()
            ..add(LoadChat()),
          child: ChatScreen(),
        ),
        BlocProvider(
          create: (context) =>
          ChatListBloc()
            ..add(LoadChatList()),
          child: ChatListPage(),
        ),
      ],
      child: MaterialApp(
        initialRoute: HomeScreen.route,
        routes: {
          HomeScreen.route: (context) => HomeScreen(),
          ChatScreen.route: (context) => ChatScreen(),
        },
      ),
    );
  }
}
