import 'package:facecatx/model/chat.dart';
import 'package:facecatx/model/chat_page.dart';

abstract class FacecatApi {
  Future<ChatPage> getChatPage({String nextFrom, int lastTs, int count});
}
