import 'package:dio/dio.dart';
import 'package:facecatx/model/chat.dart';
import 'package:facecatx/model/chat_page.dart';
import 'package:facecatx/service/facecat/facecat_api.dart';
import 'package:flutter/foundation.dart';

typedef ResultMapper<T> = T Function(dynamic data);
enum ResponseType { NOT_AUTHENTICATED, FORBIDDEN, SERVER_ERROR }

class HttpFacecatApi implements FacecatApi {
  String baseUrl;
  Dio dio;

  Map<String, dynamic> defaultQueryParams;

  HttpFacecatApi({
    this.baseUrl,
    this.dio,
    // this.challengesModel,
  }) : defaultQueryParams = {};

  @override
  Future<ChatPage> getChatPage({String nextFrom, int lastTs, int count}) async {
    var params = Map<String, dynamic>();
    if (nextFrom != null) { params["from"] = nextFrom; }
    if (lastTs != null) { params["last_ts"] = lastTs; }
    if (count != null) { params["count"] = count; }
    var response = await dio.get("$baseUrl/feed", queryParameters: params);
    //print(response);
    //var data = response.data["items"];

//    return await compute(_parsePage, response.data);
    final page = ChatPage.fromJson(response.data);
    return page;
  }

  ChatPage _parsePage(dynamic data) {
    final page = ChatPage.fromJson(data);
    return page;
  }
}
