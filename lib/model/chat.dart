import 'package:facecatx/model/cat.dart';
import 'package:facecatx/model/user.dart';
import 'package:json_annotation/json_annotation.dart';

import 'message.dart';

part 'chat.g.dart';

@JsonSerializable(nullable: true)
class Chat {
  @JsonKey(name: "chat_id")
  int id;
  @JsonKey(name: "name")
  String name;
  @JsonKey(name: "prev_from")
  String prevFrom;
  @JsonKey(name: "bg")
  int bg;
  @JsonKey(name: "multi")
  bool multi;
  @JsonKey(name: "ts")
  int ts;
  @JsonKey(name: "relevance")
  String relevance;
  @JsonKey(name: "messages_count")
  int messagesCount;
  @JsonKey(name:"cover")
  String cover;
  @JsonKey(name: "messages")
  List<Message> messages;
  @JsonKey(name: "author")
  User author;
  @JsonKey(name: "cat")
  Cat cat;

  String get authorName {
    if (cat?.name != null) {
      return cat.name;
    }

    if (author?.name != null) {
      return author.name;
    }
    return "";
  }

  String get avatarUrl {
    if (cat?.url != null) {
      return cat.url;
    }

    if (author?.url != null) {
      return author.url;
    }
    return "";
  }

  bool get isUser => author != null;
  bool get isCat => cat != null;

  Chat(
      {this.id,
      this.name,
      this.prevFrom,
      this.bg,
      this.multi,
      this.ts,
      this.relevance,
      this.messagesCount,
      this.cover,
      this.messages,
      this.author,
      });

  factory Chat.fromJson(Map<String, dynamic> json) => _$ChatFromJson(json);


  static List<Chat> fromJsonList(List<dynamic> json) =>
      json.map((item) => _$ChatFromJson(item)).toList();

  Map<String, dynamic> toJson() => _$ChatToJson(this);
}
