import 'package:json_annotation/json_annotation.dart';

part 'cat.g.dart';

@JsonSerializable(nullable: true)
class Cat {
  @JsonKey(name: "user_id")
  int id;
  @JsonKey(name: "name")
  String name;
  @JsonKey(name: "unique")
  int unique;
  @JsonKey(name: "url")
  String url;

  Cat({this.id, this.name, this.unique, this.url});

  factory Cat.fromJson(Map<String, dynamic> json) => _$CatFromJson(json);

  static List<Cat> fromJsonList(List<dynamic> json) =>
      json.map((item) => _$CatFromJson(item)).toList();

  Map<String, dynamic> toJson() => _$CatToJson(this);
}
