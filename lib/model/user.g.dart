// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    id: json['user_id'] as int,
    name: json['name'] as String,
    bio: json['bio'] as String,
    layers: json['layers'] as String,
    online: json['online'] as bool,
  );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'user_id': instance.id,
      'name': instance.name,
      'bio': instance.bio,
      'layers': instance.layers,
      'online': instance.online,
    };
