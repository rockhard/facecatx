import 'package:facecatx/di.dart';
import 'package:facecatx/model/cat.dart';
import 'package:facecatx/model/sticker.dart';
import 'package:facecatx/model/user.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:intl/intl.dart';

import 'photo.dart';

part 'message.g.dart';

@JsonSerializable(nullable: true)
class Message {
  @JsonKey(name: "msg_id")
  int id;
  @JsonKey(name: "user")
  User user;
  @JsonKey(name: "cat")
  Cat cat;
  @JsonKey(name: "text")
  String text;
  @JsonKey(name: "timestamp")
  int timestamp;
  @JsonKey(name: "sticker")
  Sticker sticker;
  @JsonKey(name: "photo")
  Photo photo;


  String get formattedTime {
    return "${readTimestamp(timestamp)}";
  }

  String readTimestamp(int timestamp) {
    var now = DateTime.now();
    var format = DateFormat('HH:mm a');
    var date = DateTime.fromMillisecondsSinceEpoch(timestamp * 1000 * 1000);
    var diff = now.difference(date);
    var time = '';

    if (diff.inSeconds <= 0 || diff.inSeconds > 0 && diff.inMinutes == 0 || diff.inMinutes > 0 && diff.inHours == 0 || diff.inHours > 0 && diff.inDays == 0) {
      time = format.format(date);
    } else if (diff.inDays > 0 && diff.inDays < 7) {
      if (diff.inDays == 1) {
        time = diff.inDays.toString() + ' DAY AGO';
      } else {
        time = diff.inDays.toString() + ' DAYS AGO';
      }
    } else {
      if (diff.inDays == 7) {
        time = (diff.inDays / 7).floor().toString() + ' WEEK AGO';
      } else {

        time = (diff.inDays / 7).floor().toString() + ' WEEKS AGO';
      }
    }

    return time;
  }

  String get name {
    if (cat?.name != null) {
      return cat.name;
    }

    if (user?.name != null) {
      return user.name;
    }
    return "";
  }

  String get avatarUrl {
    if (cat?.url != null) {
      return cat.url;
    }

    if (user?.url != null) {
      return user.url;
    }
    return "";
  }

  String get photoUrl {
    if (photo?.url != null) {
      return photo.url;
    }

    return "";
  }

  bool get isUser => user != null;
  bool get isCat => cat != null;
  bool get isImage => photo != null;
  bool get isSticker => sticker != null;

  Message({this.id, this.user, this.cat, this.text, this.timestamp, this.sticker, this.photo});

  factory Message.fromJson(Map<String, dynamic> json) =>
      _$MessageFromJson(json);

  static List<Message> fromJsonList(List<dynamic> json) =>
      json.map((item) => _$MessageFromJson(item)).toList();

  Map<String, dynamic> toJson() => _$MessageToJson(this);
}
