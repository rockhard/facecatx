import 'package:json_annotation/json_annotation.dart';

part 'photo.g.dart';

@JsonSerializable(nullable: true)
class Photo {
  @JsonKey(name: "width")
  int width;
  @JsonKey(name: "height")
  int height;
  @JsonKey(name: "url")
  String url;

  Photo({this.width, this.height, this.url});

  factory Photo.fromJson(Map<String, dynamic> json) => _$PhotoFromJson(json);

  static List<Photo> fromJsonList(List<dynamic> json) =>
      json.map((item) => _$PhotoFromJson(item)).toList();

  Map<String, dynamic> toJson() => _$PhotoToJson(this);
}