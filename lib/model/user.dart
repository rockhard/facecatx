import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable(nullable: true)
class User {
  @JsonKey(name: "user_id")
  int id;
  @JsonKey(name: "name")
  String name;
  @JsonKey(name: "bio")
  String bio;
  @JsonKey(name: "layers")
  String layers;
  @JsonKey(name: "online")
  bool online;

  String get url => "https://stickerface.io/api/png/${layers}";//?size=86";

  User({this.id, this.name, this.bio, this.layers, this.online});

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  static List<User> fromJsonList(List<dynamic> json) =>
      json.map((item) => _$UserFromJson(item)).toList();

  Map<String, dynamic> toJson() => _$UserToJson(this);
}
