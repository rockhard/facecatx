// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'message.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Message _$MessageFromJson(Map<String, dynamic> json) {
  return Message(
    id: json['msg_id'] as int,
    user: json['user'] == null
        ? null
        : User.fromJson(json['user'] as Map<String, dynamic>),
    cat: json['cat'] == null
        ? null
        : Cat.fromJson(json['cat'] as Map<String, dynamic>),
    text: json['text'] as String,
    timestamp: json['timestamp'] as int,
    sticker: json['sticker'] == null
        ? null
        : Sticker.fromJson(json['sticker'] as Map<String, dynamic>),
    photo: json['photo'] == null
        ? null
        : Photo.fromJson(json['photo'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$MessageToJson(Message instance) => <String, dynamic>{
      'msg_id': instance.id,
      'user': instance.user,
      'cat': instance.cat,
      'text': instance.text,
      'timestamp': instance.timestamp,
      'sticker': instance.sticker,
      'photo': instance.photo,
    };
