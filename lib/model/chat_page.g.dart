// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_page.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatPage _$ChatPageFromJson(Map<String, dynamic> json) {
  return ChatPage(
    chats: (json['items'] as List)
        ?.map(
            (e) => e == null ? null : Chat.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    nextFrom: json['next_from'] as String,
  );
}

Map<String, dynamic> _$ChatPageToJson(ChatPage instance) => <String, dynamic>{
      'items': instance.chats,
      'next_from': instance.nextFrom,
    };
