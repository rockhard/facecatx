import 'package:json_annotation/json_annotation.dart';

part 'sticker.g.dart';

@JsonSerializable(nullable: true)
class Sticker {
  @JsonKey(name: "user_id")
  int id;
  Sticker({this.id});

  factory Sticker.fromJson(Map<String, dynamic> json) =>
      _$StickerFromJson(json);

  static List<Sticker> fromJsonList(List<dynamic> json) =>
      json.map((item) => _$StickerFromJson(item)).toList();

  Map<String, dynamic> toJson() => _$StickerToJson(this);
}
