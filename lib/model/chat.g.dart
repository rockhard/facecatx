// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Chat _$ChatFromJson(Map<String, dynamic> json) {
  return Chat(
    id: json['chat_id'] as int,
    name: json['name'] as String,
    prevFrom: json['prev_from'] as String,
    bg: json['bg'] as int,
    multi: json['multi'] as bool,
    ts: json['ts'] as int,
    relevance: json['relevance'] as String,
    messagesCount: json['messages_count'] as int,
    cover: json['cover'] as String,
    messages: (json['messages'] as List)
        ?.map((e) =>
            e == null ? null : Message.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    author: json['author'] == null
        ? null
        : User.fromJson(json['author'] as Map<String, dynamic>),
  )..cat = json['cat'] == null
      ? null
      : Cat.fromJson(json['cat'] as Map<String, dynamic>);
}

Map<String, dynamic> _$ChatToJson(Chat instance) => <String, dynamic>{
      'chat_id': instance.id,
      'name': instance.name,
      'prev_from': instance.prevFrom,
      'bg': instance.bg,
      'multi': instance.multi,
      'ts': instance.ts,
      'relevance': instance.relevance,
      'messages_count': instance.messagesCount,
      'cover': instance.cover,
      'messages': instance.messages,
      'author': instance.author,
      'cat': instance.cat,
    };
