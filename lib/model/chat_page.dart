import 'package:facecatx/model/chat.dart';
import 'package:json_annotation/json_annotation.dart';

part 'chat_page.g.dart';

@JsonSerializable(nullable: true)
class ChatPage {
  @JsonKey(name: "items")
  List<Chat> chats;
  @JsonKey(name: "next_from")
  String nextFrom;

  ChatPage({this.chats, this.nextFrom});

  bool get isEmpty => this.chats.isEmpty;

  factory ChatPage.fromJson(Map<String, dynamic> json) =>
      _$ChatPageFromJson(json);

  static List<ChatPage> fromJsonList(List<dynamic> json) =>
      json.map((item) => _$ChatPageFromJson(item)).toList();

  Map<String, dynamic> toJson() => _$ChatPageToJson(this);
}
