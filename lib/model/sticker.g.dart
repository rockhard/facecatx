// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sticker.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Sticker _$StickerFromJson(Map<String, dynamic> json) {
  return Sticker(
    id: json['user_id'] as int,
  );
}

Map<String, dynamic> _$StickerToJson(Sticker instance) => <String, dynamic>{
      'user_id': instance.id,
    };
