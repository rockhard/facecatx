// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cat.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Cat _$CatFromJson(Map<String, dynamic> json) {
  return Cat(
    id: json['user_id'] as int,
    name: json['name'] as String,
    unique: json['unique'] as int,
    url: json['url'] as String,
  );
}

Map<String, dynamic> _$CatToJson(Cat instance) => <String, dynamic>{
      'user_id': instance.id,
      'name': instance.name,
      'unique': instance.unique,
      'url': instance.url,
    };
