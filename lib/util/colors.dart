import 'package:flutter/material.dart';

const appPinkColor = Color(0xFFE645B0);

extension BackgoundColors on int {

  Color get toPrimaryColor {
    switch (this) {
      case 0:
        return Color.fromRGBO(0, 0, 0, 0); //image
      case 1:
        return Color.fromRGBO(255, 111, 65, 1.0); //red
      case 2:
        return Color.fromRGBO(255, 190, 64, 1.0); //orange
      case 3:
        return Color.fromRGBO(168, 217, 76, 1.0); //green
      case 4:
        return Color.fromRGBO(76, 216, 217, 1.0); //sea
      case 5:
        return Color.fromRGBO(89, 157, 255, 1.0); //blue
      case 6:
        return Color.fromRGBO(199, 89, 217, 1.0); //purple
    }
    return Color.fromRGBO(0, 0, 0, 0);
  }

  Color get toSecondaryColor {
    switch (this) {
      case 0:
        return Color.fromRGBO(0, 0, 0, 0.6); //image
      case 1:
        return Color.fromRGBO(243, 38, 71, 1.0); //red
      case 2:
        return Color.fromRGBO(243, 107, 37, 1.0); //orange
      case 3:
        return Color.fromRGBO(81, 180, 46, 1.0); //green
      case 4:
        return Color.fromRGBO(49, 145, 192, 1.0); //sea
      case 5:
        return Color.fromRGBO(70, 71, 231, 1.0); //blue
      case 6:
        return Color.fromRGBO(118, 59, 231, 1.0); //purple
    }
    return Color.fromRGBO(0, 0, 0, 0);
  }
}
