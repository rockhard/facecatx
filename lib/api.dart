import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:dio/dio.dart';
import 'package:facecatx/di.dart';

import 'service/facecat/facecat_api.dart';

class ApiService {

  Future<void> testMethod() async {
    //Response response;

    // try {
    //   var response = await _dio.request(
    //     "/auth/direct",
    //     options: Options(
    //       method: "POST",
    //       contentType: "application/json",
    //     ),
    //     data: {
    //       'nonce': '1587824524221',
    //       'sign': '1beceb24e802a1bbdff45604e0eca361'
    //     },
    //   );
    //   return Future<String>.value('Response body: ${response.toString()}');
    // } on DioError catch (e) {
    //   print(e);
    //   return Future<String>.value('Response error: ${e.toString()}');
    // }

//    return await _dio.post(
//      "/auth/direct",
//      options: Options(
//        method: "POST",
//        contentType: "application/json",
//      ),
//      data: {
//        'nonce': '1587824524221',
//        'sign': '1beceb24e802a1bbdff45604e0eca361'
//      },
//    );
//
//
//    try {
//      //404
//      await dio.get("https://wendux.github.io/xsddddd");
//    } on DioError catch (e) {
//      // The request was made and the server responded with a status code
//      // that falls out of the range of 2xx and is also not 304.
//      if (e.response) {
//        print(e.response.data)
//        print(e.response.headers)
//        print(e.response.request)
//      } else {
//        // Something happened in setting up or sending the request that triggered an Error
//        print(e.request)
//        print(e.message)
//      }
//    }
//
//    var url = 'https://face.cat/api/auth/direct';
//
//    var nonce = generateNonce();
//    print("nonce: $nonce");
//    var sign = generateSign(nonce);
//    print("sign: $sign");
//
////    var data = {
////      'nonce': nonce,
////      'sign': sign,
////    };
//
//    var data = {
//      'nonce': '1587824524221',
//      'sign': '1beceb24e802a1bbdff45604e0eca361'
//    };
//
//    var body = json.encode(data);
//
//    var response = await Client().post(
//      url,
//      headers: {"Content-Type": "application/json"},
//      body: body,
//    );
//
//    print('Response status: ${response.statusCode}');
//    print('Response body: ${response.body}');
//
//    Map decoded = json.decode(response.body);
//
//    String token = decoded['token'];
//    print(token);
//
//    _token = token;
//
//    return Future<String>.value('Response body: ${response.body}');
  }
}

extension CryptoExtensions on String {
  String md5Digest() {
    return _generateMd5(this);
  }

  String _generateMd5(String input) {
    return md5.convert(utf8.encode(input)).toString();
  }

//  String generateNonce() {
//    return DateTime.now().millisecondsSinceEpoch.toString();
//  }
//
//  String generateSign(String nonce) {
//    return (nonce + Url.definetlyNotAnAppSecretKey).md5Digest();
//  }
}
