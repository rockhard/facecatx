import 'dart:io';

import 'package:dio/dio.dart';
import 'package:facecatx/service/facecat/facecat_api.dart';
import 'package:facecatx/service/facecat/http_facecat_api.dart';
import 'package:facecatx/service/login_service.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';

import 'service/registration_service.dart';

final locator = GetIt.asNewInstance();

class DependencyInjection {
  static Future<void> setUp() async {
    await DotEnv().load('.env');
    await _setUpServices();
  }

  static String token;

  static Future<void> _setUpServices() async {

    var dio = Dio(BaseOptions(
      receiveDataWhenStatusError: true,
      connectTimeout: 5000,
      receiveTimeout: 5000,
    ));

    dio = addInterceptors(dio);

    locator.registerSingleton<FacecatApi>(HttpFacecatApi(
      baseUrl: DotEnv().env['BASE_URL'],
      dio: dio,
    ));

    final SharedPreferences storage = await SharedPreferences.getInstance();
    storage.setString("token", "1af706150e09addaa69e8f030830dc24d41d8");
    token = storage.get("token");

    locator.registerSingleton(storage);

    locator.registerSingleton<HttpRegistrationService>(HttpRegistrationService(
      dio: dio,
      baseUrl: DotEnv().env['BASE_URL'],
    ));
    locator.registerSingleton<HttpLoginService>(HttpLoginService(
      dio: dio,
      baseUrl: DotEnv().env['BASE_URL'],
    ));

    Intl.defaultLocale = 'ru_RU';
    await initializeDateFormatting('ru_RU', null);
  }

  static Dio addInterceptors(Dio dio) {
    return dio
      ..interceptors.add(InterceptorsWrapper(
          onResponse: (Response response) => responseInterceptor(response),
          onRequest: (RequestOptions options) => requestInterceptor(options),
          onError: (DioError dioError) => dioError));
  }

  static dynamic responseInterceptor(Response options) async {
    print(options);
    if (options.request.method != 'POST') return options;
//    if (options.statusCode < 299) {
//      if ((options.request.path.endsWith("/auth/login") ||
//          options.request.path.endsWith("/auth/register"))) {
    var data = options.data as Map<String, dynamic>;
    if (data['token'] == null) return options;
    token = data['token'];

    final SharedPreferences storage = await SharedPreferences.getInstance();
    storage.setString("token", token);

//        if (data['user'] == null) return options;
//        User user = User.fromJson(data['user']);
//        locator
//            .get<FlutterSecureStorage>()
//            .write(key: "user", value: jsonEncode(user.toJson()));
//      }
//    }
    return options;
  }

  static dynamic requestInterceptor(RequestOptions options) {
    if (token == null) return options;

    options.headers.addAll({
      "Content-Type": "application/json",
      "X-Token": token,
    });

    //options.headers.addAll({"Authorization": "Bearer " + token});
    return options;
  }
}
